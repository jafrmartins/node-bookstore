# Node Bookstore

![doodle](https://gitlab.com/jafrmartins/node-bookstore/raw/master/doodle.png)

## Install
<pre>
git clone https://github.com/jafrmartins/node-bookstore.git
cd node-bookstore
npm install
gulp install
</pre>

## Start
<pre>
gulp start
</pre>

## Test
<pre>
gulp test
</pre>

## Stop
<pre>
gulp stop
</pre>
