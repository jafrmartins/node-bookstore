express = require 'express'
mongoose = require 'mongoose'
bodyParser = require 'body-parser'
mkdirp = require 'mkdirp'
cors = require 'cors'
io = require 'socket.io'

{ resolve } = require 'path'
{ createServer } = require 'http'
{ writeFileSync: write } = require 'fs'
{ v4: uuid } = require 'node-uuid'
{ config } = require resolve process.cwd(), 'package.json'

process.on "uncaughtException", (err) -> console.log err

mongoose.connect config.mongodb_url

app = express()
server = createServer app

app.use cors()
app.use bodyParser.json()
app.use bodyParser.urlencoded({ extended: true })
io = io server

app.use '/api/v1/books', require './books'

[ "books" ].map (resource) ->
  [ "put", "post", "del" ].map (method) ->
    process.on "#{method}:#{resource}", (data) ->
      Object.keys(app.sockets).map (id) ->
        socket = app.sockets[id]
        socket.broadcast.emit "#{method}:#{resource}", data

app.sockets = {}
io.on 'connection', (ws) ->  app.sockets[ws.id] = ws

server.listen process.env.PORT or config.express_port

mkdirp resolve config.express_path
pidfile = resolve config.express_path, config.express_pidfile
write pidfile, process.pid

module.exports = app
