del = require 'del'
gulp = require 'gulp'
shell = require 'gulp-shell'
series = require 'run-sequence'
{ config } = require './package'

require './tasks/mongod'
require './tasks/mocha'
require './tasks/node'
require './tasks/debian'

gulp.task 'default', [
  'start:node'
]
