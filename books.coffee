{ Router } = require 'express'
router = new Router
mongoose = require 'mongoose'
{ Schema } = mongoose

schema = new Schema {
  title: { type: "String", required: true }
  author: { type: "String", required: true }
}

model = mongoose.model 'books', schema

resource = require './resource'

router.get '/', resource.find.bind(

  channel: "find:books", model: model, ee: process

)

router.get '/:id', resource.get.bind(

  channel: "get:books", model: model, ee: process

)

router.post '/', resource.post.bind(

  channel: "post:books", model: model, ee: process

)

router.put '/:id', resource.put.bind(

  channel: "put:books", model: model, ee: process

)

router.delete '/:id', resource.delete.bind(

  channel: "del:books", model: model, ee: process

)

module.exports = router
